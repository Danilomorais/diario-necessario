import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

// Natives
import { Firebase } from '@ionic-native/firebase';
import { HomePageModule } from '../pages/home/home.module';
import { ActionPageModule } from '../pages/action/action.module';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs/tabs';
import { RegisterModule } from '../pages/register/register.module';
import { ResetpwdModule } from '../pages/resetpwd/resetpwd.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Import the AF2 Module
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

//Services
import { AuthService } from '../providers/auth-service';
import { DailyService } from '../providers/daily-service';

//Pipes
import { PipesModule } from '../pipes/pipes.module'

// Plugins
import { Facebook } from '@ionic-native/facebook'

export const firebaseConfig = {
  apiKey: "AIzaSyBSMUs6RVQOx-r-tIRiUHXjJ8dio6fPfto",
  authDomain: "diario-necessario.firebaseapp.com",
  databaseURL: "https://diario-necessario.firebaseio.com",
  projectId: "diario-necessario",
  storageBucket: "diario-necessario.appspot.com",
  messagingSenderId: "990000766651"
};


@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    TabsPage
  ],
  imports: [
    PipesModule,
    HomePageModule,
    ActionPageModule,
    RegisterModule,
    ResetpwdModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Facebook,
    AuthService,
    DailyService,
    Firebase,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}

import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import firebase from 'firebase/app';

@Injectable()
export class AuthService {
  public userData: any;

  constructor(private af: AngularFireAuth) {
    this.userData = firebase.database().ref('/userData');
  }

  loginUser(email: string, password: string): firebase.Promise<any> {
    return this.af.auth.signInWithEmailAndPassword(email, password);
  }
  
  resetPassword(email: string): firebase.Promise<any> {
    return this.af.auth.sendPasswordResetEmail(email);
  }

  logoutUser(): firebase.Promise<any> {
    return this.af.auth.signOut();
  }

  register(email: string, password: string, name: string): firebase.Promise<any> {
    return this.af.auth.createUserWithEmailAndPassword(email, password).then((newUser) => {
      this.userData.child(newUser.uid).set({
        username: name,
        email: email
      });
    });
  }

  updateProfile(userInfo): firebase.Promise<any> {
    return this.af.auth.currentUser.updateProfile(userInfo);
  }

  getLoggedUserInfo() {
    return this.af.auth.currentUser;
  }
}

import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import firebase from 'firebase/app';


@Injectable()
export class DailyService {
  private dailyList: FirebaseListObservable<any>;
  private itemValue: FirebaseObjectObservable<any>;
  private todoList: FirebaseListObservable<any[]>;
  private gratitudeOfTheDay: any; 
  private date: string;  

  constructor(private db: AngularFireDatabase, private auth: AngularFireAuth) {
    this.date = new Date().toISOString().split("T")[0];

    this.todoList = this.db.list('/daily/' + this.getLoggedUserUID() + "/" + this.date + "/actions");
    this.dailyList = this.db.list('/daily/' + this.getLoggedUserUID());
  }

  addGratitudeByDate(dailyGratitude: string, todoList: string[], date: string): firebase.Promise<any> {
      return this.dailyList.update(date.split("T")[0],{
        gratitude: dailyGratitude,
        actions: todoList
      });
  }

  getTodoList(){
    return this.todoList;
  }

  getLoggedUserUID() {
    return this.auth.auth.currentUser.uid;
  }

  getAllGratitude() {
    return this.gratitudeOfTheDay = this.db.list('/daily/' + this.getLoggedUserUID());
  }

  updateAction(index, value): firebase.Promise<any> {
    this.itemValue = this.db.object('/daily/' + this.getLoggedUserUID() + "/" + this.date + "/actions/" + index);
    return this.itemValue.update({ value: value });
  }

  filterItems(searchTerm){
    
    return this.gratitudeOfTheDay.filter((item) => {
        return item.$key.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });    
        
  }

}

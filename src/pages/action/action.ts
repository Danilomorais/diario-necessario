import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { DailyService } from '../../providers/daily-service';

@Component({
  selector: 'page-action',
  templateUrl: 'action.html',
})
export class ActionPage {
  private todoList;
  tomorrow = new Date();  
  private tomorrowString: string;
  
  constructor(public navCtrl: NavController, afDB: AngularFireDatabase, private dailyService: DailyService) {
    this.todoList = dailyService.getTodoList();
    this.tomorrow.setDate(this.tomorrow.getDate() + 1);

    this.tomorrowString = this.tomorrow.toISOString().split('T')[0];
    this.tomorrowString = this.tomorrowString.substr(8, 2) +'/'+ this.tomorrowString.substr(5, 2);
  }

  toggleCheck(index, value){
    value = !value;
    this.dailyService.updateAction(index, value);
  }
}
import { Component } from '@angular/core';
import { NavController, AlertController, LoadingController, IonicPage } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { LoginPage } from '../login/login';
import { AuthService } from '../../providers/auth-service';
import { DailyService } from '../../providers/daily-service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  todo: string;
  todos: any = [];
  dailyGratitude: string;
  name: string;
  date: String;
  dailyForm: FormGroup;
  loading: any;
  todoChanged: boolean = false;
  submitAttempt: boolean = false;
  gratitudeOfTheDay: any;
  
  constructor(public navCtrl: NavController, afDB: AngularFireDatabase, private authService: AuthService,
              private dailyService: DailyService, public alertCtrl: AlertController,
              public loadingCtrl: LoadingController, public formBuilder: FormBuilder) {
    this.name = this.authService.getLoggedUserInfo().displayName;

    this.dailyForm = formBuilder.group({
      dailyGratitude: ['', Validators.compose([Validators.required])]
    });
    
    this.date = new Date().toISOString();

  }
  
  ionViewWillEnter() {
    this.getGratitudeByDate(this.date);
  }
  
  getGratitudeByDate(date){
    this.date = date;
    this.dailyService.getAllGratitude()
    .subscribe((_gratitudeList) => {
      this.gratitudeOfTheDay = '';
      this.todos = [];
       _gratitudeList.forEach(item => {
         if(item.$key.toLowerCase().indexOf(date.split("T")[0].toLowerCase()) > -1){
           this.gratitudeOfTheDay = item.gratitude;
           if(item.actions)
           this.todos = item.actions;
          }    
      });
    })
  }

  elementChanged(input){
    let field = input.ngControl.name;
    this[field + "Changed"] = true;
  }
  
  addAction() {
    if(this.todo){
      this.todos.push({
        key:   this.todo,
        value: false
    });
      this.todo = "";
    }
  }

  save(date) {
    this.submitAttempt = true;
    if (!this.dailyForm.valid){
      console.log(this.dailyForm.value, this.todos);
    } else {
      this.dailyService.addGratitudeByDate(this.dailyForm.value.dailyGratitude, this.todos, date)
      .then( data => {
          this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: "Gratidão cadastrada com sucesso",
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      }, error => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: "Ocorreu um erro, tente novamente",
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });
  
      this.loading = this.loadingCtrl.create({
        dismissOnPageChange: true,
      });
      this.loading.present();
    }    
  }

  delete(item) {
    var index = this.todos.indexOf(item, 0);
    if (index > -1) {
      this.todos.splice(index, 1);
    }
  }

  logout() {
    this.authService.logoutUser();
    this.navCtrl.push(LoginPage);
  }
}

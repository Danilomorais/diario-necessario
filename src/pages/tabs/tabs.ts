import { Component } from '@angular/core';

import { ActionPage } from '../action/action';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ActionPage;

  constructor() {

  }
}
